#!/usr/bin/env bash

ABSOLUTE_PATH="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
echo "ABSOLUTE PATH : "$ABSOLUTE_PATH
export PATH="$PATH:$ABSOLUTE_PATH/../Dependencies/Linux-toolchain-xtensa-esp32-elf/bin"
export IDF_PATH="$ABSOLUTE_PATH/../Dependencies/esp-idf"
printenv PATH
printenv IDF_PATH

cd $ABSOLUTE_PATH/../Software/

make size-components
