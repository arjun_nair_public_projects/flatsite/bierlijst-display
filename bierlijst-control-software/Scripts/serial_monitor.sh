#!/usr/bin/env bash

cd ..

ABSOLUTE_PATH="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
echo "ABSOLUTE PATH : "$ABSOLUTE_PATH
export PATH="$PATH:$ABSOLUTE_PATH/Dependencies/Linux-toolchain-xtensa-esp32-elf/tools/xtensa-esp32-elf/esp32-2019r1-8.2.0/xtensa-esp32-elf/bin/:$ABSOLUTE_PATH/Dependencies/xtensa-esp32-elf/bin/"
export PATH="$PATH:$ABSOLUTE_PATH/Dependencies/esp-idf"
export IDF_PATH="$ABSOLUTE_PATH/Dependencies/esp-idf"
printenv PATH
printenv IDF_PATH

cd $ABSOLUTE_PATH/Software/

make monitor
