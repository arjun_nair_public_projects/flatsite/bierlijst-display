#!/usr/bin/env bash

ABSOLUTE_PATH="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
echo "ABSOLUTE PATH : "$ABSOLUTE_PATH
export PATH="$PATH:$ABSOLUTE_PATH/Dependencies/Linux-toolchain-xtensa-esp32-elf/tools/xtensa-esp32-elf/esp32-2019r1-8.2.0/xtensa-esp32-elf/bin/"
export IDF_PATH="$ABSOLUTE_PATH/../Dependencies/esp-idf"
export IDF_TOOLS_PATH="$ABSOLUTE_PATH/../Dependencies/Linux-toolchain-xtensa-esp32-elf"
printenv PATH
printenv IDF_PATH
printenv IDF_TOOLS_PATH

cd $ABSOLUTE_PATH/../
mkdir -p Dependencies
cd Dependencies
git clone -b v3.3 --recursive https://github.com/espressif/esp-idf.git

## For v3.x.x.
wget https://dl.espressif.com/dl/xtensa-esp32-elf-linux64-1.22.0-80-g6c4433a-5.2.0.tar.gz
tar -xzf xtensa-esp32-elf-linux64-1.22.0-80-g6c4433a-5.2.0.tar.gz
rm -f xtensa-esp32-elf-linux64-1.22.0-80-g6c4433a-5.2.0.tar.gz


## When v4.0 and above is used.
#./esp-idf/install.sh
