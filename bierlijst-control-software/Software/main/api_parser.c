#include "inc/api_parser.h"

#include "inc/wifi_manager.h"

typedef struct s_api_parser_person
{
	uint32_t id;
	char name[100];
	uint16_t beers;
	int16_t status;
	int16_t turns;
} t_api_parser_person;


esp_http_client_config_t api_parser_config;
uint8_t api_parser_request_running = 0;
t_api_parser_person people[API_PARSER_CONSTANT_NUMBER_OF_PEOPLE];
char api_parser_current_http_data[8000];
uint32_t api_parser_current_http_data_pos = 0;
uint8_t api_parser_button_pressed = 0;

esp_err_t _http_event_handler(esp_http_client_event_t *evt);

void API_PARSER_INIT()
{
	api_parser_config.url = "http://httpbin.org/get";
	api_parser_config.event_handler = _http_event_handler;
	
	for(int i = 0; i < API_PARSER_CONSTANT_NUMBER_OF_PEOPLE; i++)
	{
		people[i].id = 0;
		people[i].name[0] = 0;
		people[i].beers = 0;
		people[i].status = 0;
		people[i].turns = 0;
	}
	// Setup people.
	strcpy(people[0].name, "Arjun");
	strcpy(people[1].name, "Brian");
	strcpy(people[2].name, "Charlotte");
	strcpy(people[3].name, "Dylan");
	strcpy(people[4].name, "Edwin");
	strcpy(people[5].name, "Lea");
	strcpy(people[6].name, "Naomi");
	strcpy(people[7].name, "Renske");
	strcpy(people[8].name, "Rik");
}

void API_PARSER_LOOP()
{
	static uint16_t timer = 0;
	char post_data[100] = "";
		
	if(timer)
	{
		timer--;
	}
	else
	{
		timer = 200;
	}
	
	uint8_t more_to_add = 0;
	if(api_parser_button_pressed)
	{
		for(int i = 0; i < API_PARSER_CONSTANT_NUMBER_OF_PEOPLE; i++)
		{
			if(people[i].turns)
			{
 				sprintf(post_data, "{\"request\":\"add\",\"id\":\"%d\",\"add_beers\":\"%d\"}", people[i].id, people[i].turns);
				printf("Sending data [ %s ].\n", post_data);
				people[i].turns = 0;
				more_to_add = 1;
				break;
			}
		}
	}
	if(more_to_add == 0)
	{
		api_parser_button_pressed = 0;
	}
	
	if(
		( (timer == 0) || (api_parser_button_pressed) )
		&& (WIFI_MANAGER_IS_CONNECTED())
		&& (api_parser_request_running == 0)
	)
	{
		api_parser_request_running = 1;
		api_parser_current_http_data_pos = 0;
		esp_http_client_handle_t client = esp_http_client_init(&api_parser_config);


		// POST
		esp_http_client_set_url(client, "http://192.168.0.103:8000/bierlijst/api");
		esp_http_client_set_method(client, HTTP_METHOD_POST);
		esp_http_client_set_post_field(client, post_data, strlen(post_data));
		esp_err_t err = esp_http_client_perform(client);
		if (err == ESP_OK) 
		{
			printf("HTTP POST Status = %d, content_length = %d.\n",
					esp_http_client_get_status_code(client),
					esp_http_client_get_content_length(client));
		}
		else
		{
			printf("HTTP POST request failed: %s.\n", esp_err_to_name(err));
		}

		esp_http_client_cleanup(client);
	}
}


uint16_t API_PARSER_GET_BEERS(uint32_t index)
{
	uint16_t ret_val = 0;
	if(index < API_PARSER_CONSTANT_NUMBER_OF_PEOPLE)
	{
		ret_val = people[index].beers;
	}
	
	return ret_val;
}


int16_t API_PARSER_GET_STATUS(uint32_t index)
{
	int16_t ret_val = 0;
	if(index < API_PARSER_CONSTANT_NUMBER_OF_PEOPLE)
	{
		ret_val = people[index].status;
	}
	
	return ret_val;
}


void API_PARSER_SET_TURNS(uint32_t index, int16_t up_nDown)
{
	if(index < API_PARSER_CONSTANT_NUMBER_OF_PEOPLE)
	{
		if(up_nDown)
		{
			people[index].turns++;
		}
		else if(people[index].turns)
		{
			people[index].turns--;
		}
		printf("%d . \n", index);
	}
}

int16_t API_PARSER_GET_TURNS(uint32_t index)
{
	int16_t ret_val = 0;
	if(index < API_PARSER_CONSTANT_NUMBER_OF_PEOPLE)
	{
		ret_val = people[index].turns;
	}
	
	return ret_val;
}

void API_PARSER_HANDLE_BUTTON()
{
	api_parser_button_pressed = 1;
}

esp_err_t _http_event_handler(esp_http_client_event_t *evt)
{
	switch(evt->event_id) {
		case HTTP_EVENT_ERROR:
			printf("HTTP_EVENT_ERROR\n");
			api_parser_request_running = 0;
			break;
		case HTTP_EVENT_ON_CONNECTED:
			//printf("HTTP_EVENT_ON_CONNECTED\n");
			break;
		case HTTP_EVENT_HEADER_SENT:
			//printf("HTTP_EVENT_HEADER_SENT\n");
			break;
		case HTTP_EVENT_ON_HEADER:
		//	printf("HTTP_EVENT_ON_HEADER, key=%s, value=%s.\n", evt->header_key, evt->header_value);
			break;
		case HTTP_EVENT_ON_DATA:
		//	printf("HTTP_EVENT_ON_DATA, len=%d.\n", evt->data_len);
			if (!esp_http_client_is_chunked_response(evt->client)) 
			{
				// Write out data
				sprintf(api_parser_current_http_data + api_parser_current_http_data_pos, "%.*s\n\n\n", evt->data_len, (char*)evt->data);
				api_parser_current_http_data_pos += evt->data_len;
				
			}
			break;
		case HTTP_EVENT_ON_FINISH:
			//   printf("HTTP_EVENT_ON_FINISH\n");
			api_parser_current_http_data[api_parser_current_http_data_pos] = 0;
			
			cJSON *root = cJSON_Parse(api_parser_current_http_data);
			cJSON *tag_data = cJSON_GetObjectItem(root,"data");
			uint16_t tag_data_array_length = cJSON_GetArraySize(tag_data);
			//printf("Array length : %d.\n\n", tag_data_array_length);
			
			for(int i = 0; i < tag_data_array_length; i++)
			{
				cJSON *tag_person = cJSON_GetArrayItem(tag_data, i);
				
				uint8_t person_found = 0;
				for(int j = 0; j < API_PARSER_CONSTANT_NUMBER_OF_PEOPLE; j++)
				{
					if(strcmp(people[j].name, cJSON_GetObjectItem(tag_person,"name")->valuestring) == 0)
					{
						people[j].id = cJSON_GetObjectItem(tag_person,"id")->valueint;
						people[j].beers = cJSON_GetObjectItem(tag_person,"beers")->valueint;
						people[j].status = cJSON_GetObjectItem(tag_person,"status")->valueint;
						person_found = 1;
						break;
					}
				}
				/* printf
				(
					"%c%.3d | %-10s | %d\n",
					person_found?'*':' ',
					cJSON_GetObjectItem(tag_person,"id")->valueint,
					cJSON_GetObjectItem(tag_person,"name")->valuestring,
					cJSON_GetObjectItem(tag_person,"beers")->valueint
				); */
			}
			
			cJSON_Delete(root);
			
			api_parser_request_running = 0;
			break;
		case HTTP_EVENT_DISCONNECTED:
			// printf("HTTP_EVENT_DISCONNECTED\n");
			api_parser_request_running = 0;
			break;
	}
	return ESP_OK;
}
