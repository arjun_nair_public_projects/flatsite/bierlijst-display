#ifndef _WIFI_MANAGER_H_
#define _WIFI_MANAGER_H_

/* INCLUDES */
#include "global.h"

/* CONSTANTS */

/* FUNCTIONS */
void WIFI_MANAGER_INIT();
uint8_t WIFI_MANAGER_IS_CONNECTED();

#endif /* _WIFI_MANAGER_H_ */
