#ifndef SOURCE_INCLUDE_GLOBAL_H_
#define SOURCE_INCLUDE_GLOBAL_H_

/* Global includes */
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/event_groups.h"
#include "driver/adc.h"
#include "driver/gpio.h"
#include "driver/ledc.h"
#include "driver/mcpwm.h"
#include "driver/periph_ctrl.h"
#include "driver/timer.h"
#include "driver/uart.h"
#include "hwcrypto/aes.h"
#include "soc/mcpwm_reg.h"
#include "soc/mcpwm_struct.h"
#include "soc/timer_group_struct.h"
#include "tcpip_adapter.h"
#include "esp_bt.h"
#include "esp_ota_ops.h"
#include "esp_event.h"
#include "esp_event_loop.h"
#include "esp_flash_partitions.h"
#include "esp_partition.h"
#include "esp_gap_ble_api.h"
#include "esp_gattc_api.h"
#include "esp_gatts_api.h"
#include "esp_gatt_defs.h"
#include "esp_bt_defs.h"
#include "esp_bt_main.h"
#include "esp_event_loop.h"
#include "esp_system.h"
#include "esp_http_client.h"
#include "esp_gatt_common_api.h"
#include "esp_system.h"
#include "esp_int_wdt.h"
#include "esp_log.h"
#include "esp_task_wdt.h"
#include "esp_spi_flash.h"
#include "esp_wifi.h"
#include "esp_http_server.h"
#include "nvs_flash.h"
#include "nvs.h"
#include "lwip/err.h"
#include "lwip/sys.h"
#include "cJSON.h"

/* Defines. */
#define HASH                                             ("###########################################\n")


/* User code includes */

/* Global variables. */
extern volatile uint64_t system_time_ms;
extern volatile uint32_t system_cpu_usage;
extern volatile uint32_t system_logging_state;




/* MACRO Magic. */
/* No configuration has to be changed here. This part
 * manages configuration settings.
 */




/* Global macros. */
/* Create linearisation parameters.
 * Parameters :
 * 	X1         - Input.
 * 	Y1         - Input.
 * 	X2         - Input.
 * 	Y2         - Input.
 * 	OFFSET     - Output.
 * 	MULTIPLIER - Output.
 * 	DIVIDER    - Output.
 */
#define GLOBAL_MACRO_create_linear_parameters(X1, Y1, X2, Y2, OFFSET, MULTIPLIER, DIVIDER) \
{ \
	MULTIPLIER = (Y2) - (Y1); \
	DIVIDER    = (X2) - (X1); \
	if(DIVIDER == 0) \
	{ \
		DIVIDER = 1; \
	} \
	OFFSET = (Y1) - ((MULTIPLIER * X1) / DIVIDER); \
};
/** Minimum value.
 *
 */
#define GLOBAL_MACRO_min(_va, _vb)             ( ((_va) < (_vb)) ? (_va) : (_vb) )
/**
 * Set bit.
 *  _WORD_ The word.
 *  _BIT_  The bit number.
 */
#define GLOBAL_MACRO_SET_BIT(_WORD_, _BIT_)    ( (_WORD_) |= (1 << (_BIT_)) )
/**
 * Unset bit.
 *  _WORD_ The word.
 *  _BIT_  The bit number.
 */
#define GLOBAL_MACRO_UNSET_BIT(_WORD_, _BIT_)  ( (_WORD_) &= ~(1 << (_BIT_)) )
/**
 * Mask bit.
 *  _WORD_ The word.
 *  _BIT_  The bit number.
 */
#define GLOBAL_MACRO_MASK_BIT(_WORD_, _BIT_)  ( (_WORD_) & (1 << (_BIT_)) )
/**
 * Suppress unused warning.
 *  _VAR_ The variable.
 */
#define GLOBAL_MACRO_UNUSED(_VAR_)  ( (void)_VAR_ )
/** Debug output macros.
 *
 */
#if (GLOBAL_PRINT_DEBUG_MESSAGE == TRUE)
	#define printf_I(f_, ...) \
	{ \
		if(system_logging_state == 1) \
		{ \
			printf("\033[0;37m[   -   ] "); \
			printf((f_), ##__VA_ARGS__); \
			printf("\033[0m\n"); \
		} \
	};
	#define printf_W(f_, ...) \
	{ \
		if(system_logging_state == 1) \
		{ \
			printf("\033[0;93m[WARNING] "); \
			printf((f_), ##__VA_ARGS__); \
			printf("\033[0m\n"); \
		} \
	};
	#define printf_E(f_, ...) \
	{ \
		if(system_logging_state == 1) \
		{ \
			printf("\033[0;31m[ ERROR ] "); \
			printf((f_), ##__VA_ARGS__); \
			printf("\033[0m\n"); \
		} \
	};
#else
	#define printf_I(f_, ...)
	#define printf_W(f_, ...)
	#define printf_E(f_, ...)
#endif

#endif /* SOURCE_INCLUDE_GLOBAL_H_ */
