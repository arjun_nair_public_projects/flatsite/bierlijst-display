#ifndef _BUTTON_MANAGER_H_
#define _BUTTON_MANAGER_H_

/* INCLUDES */
#include "global.h"

/* CONSTANTS */

/* FUNCTIONS */
void BUTTON_MANAGER_INIT();
void BUTTON_MANAGER_LOOP();

#endif /* _BUTTON_MANAGER_H_ */
