#ifndef _COMMUNICATION_H_
#define _COMMUNICATION_H_

/* INCLUDES */
#include "global.h"

/* CONSTANTS */

/* FUNCTIONS */
void COMMUNICATION_INIT();
void COMMUNICATION_LOOP();

#endif /* _COMMUNICATION_H_ */
