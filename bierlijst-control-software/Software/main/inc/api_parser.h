#ifndef _API_PARSER_H_
#define _API_PARSER_H_

/* INCLUDES */
#include "global.h"

/* CONSTANTS */
#define API_PARSER_CONSTANT_NUMBER_OF_PEOPLE   (10)

/* FUNCTIONS */
void API_PARSER_INIT();
void API_PARSER_LOOP();
uint16_t API_PARSER_GET_BEERS(uint32_t index);
int16_t API_PARSER_GET_STATUS(uint32_t index);
void API_PARSER_SET_TURNS(uint32_t index, int16_t up_or_down);
int16_t API_PARSER_GET_TURNS(uint32_t index);
void API_PARSER_HANDLE_BUTTON();

#endif /* _API_PARSER_H_ */
