#include "inc/global.h"

#include "inc/api_parser.h"
#include "inc/button_manager.h"
#include "inc/communication.h"
#include "inc/wifi_manager.h"

/* Local functions */
void scheduler_timer_setup();
void IRAM_ATTR timer_group0_isr(void *trigger);

/* Local variables */
uint32_t scheduler_trigger_10ms;
uint64_t scheduler_timer_alarm_value, scheduler_cpu_usage_avg_sum;

void SCHEDULER_run()
{
	scheduler_timer_setup();
	
	API_PARSER_INIT();
	BUTTON_MANAGER_INIT();
	COMMUNICATION_INIT();
	WIFI_MANAGER_INIT();

	while(1)
	{
		if(scheduler_trigger_10ms)
		{
			scheduler_trigger_10ms = 0;
			
			
			// TASKS.
			API_PARSER_LOOP();
			BUTTON_MANAGER_LOOP();
			COMMUNICATION_LOOP();
			
			/* Report processor usage. */
			uint64_t tc;
			timer_get_counter_value(TIMER_GROUP_0, TIMER_0, &tc);
			system_cpu_usage = (uint32_t)((tc * 100) / scheduler_timer_alarm_value);
		}
	}
}


void scheduler_timer_setup()
{
	timer_config_t config;
	const uint64_t timer_divider = 16;
	scheduler_timer_alarm_value = (TIMER_BASE_CLK / timer_divider) / 100;     // (1 / 100) seconds.

	config.divider = timer_divider;
	config.counter_dir = TIMER_COUNT_UP;
	config.counter_en = TIMER_PAUSE;
	config.alarm_en = TIMER_ALARM_EN;
	config.intr_type = TIMER_INTR_LEVEL;
	config.auto_reload = TIMER_AUTORELOAD_EN;
	timer_init(TIMER_GROUP_0, 0, &config);

	/* Timer's counter will initially start from value below.
	   Also, if auto_reload is set, this value will be automatically reload on alarm */
	timer_set_counter_value(TIMER_GROUP_0, 0, 0x00000000ULL);

	timer_set_alarm_value(TIMER_GROUP_0, TIMER_0, scheduler_timer_alarm_value);
	timer_enable_intr(TIMER_GROUP_0, TIMER_0);
	timer_isr_register(TIMER_GROUP_0, TIMER_0, timer_group0_isr, NULL, ESP_INTR_FLAG_IRAM, NULL);
	timer_start(TIMER_GROUP_0, TIMER_0);
}

void IRAM_ATTR timer_group0_isr(void *par)
{
	scheduler_trigger_10ms = 1;
	system_time_ms += 10;
	TIMERG0.int_clr_timers.t0 = 1;
	TIMERG0.hw_timer[TIMER_0].config.alarm_en = TIMER_ALARM_EN;
}
