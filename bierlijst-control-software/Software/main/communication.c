#include "inc/communication.h"

#include "inc/api_parser.h"

#define COMMUNICATION_UART_RX_BUFFER_SIZE           (1024)

/** MESSAGES.
 * Byte 0 :
 *  Bit 0-3 : Address.
 *  Bit 4-6 : Command.
 *  Bit   7 : 0.
 * Byte 1  (Optional) :
 *  Bit 0-6 : Data.
 *  Bit   7 : 1.
 **/
// Commands.
#define COMMUNICATION_COMMAND_ASSIGN_ADDRESS     (0x0)
#define COMMUNICATION_COMMAND_DIGIT_0            (0x1)
#define COMMUNICATION_COMMAND_DIGIT_1            (0x2)
#define COMMUNICATION_COMMAND_DIGIT_2            (0x3)
#define COMMUNICATION_COMMAND_DIGIT_3            (0x4)
#define COMMUNICATION_COMMAND_COLOUR_RED         (0x5)
#define COMMUNICATION_COMMAND_COLOUR_GREEN       (0x6)
#define COMMUNICATION_COMMAND_COLOUR_BLUE        (0x7)
// Response.
#define COMMUNICATION_COMMAND_RESONSE_BUTTON     (0x1)


uint8_t communication_setup_address_done;
uint8_t communication_number_of_modules, communication_current_module;


uint8_t communication_create_command_byte(uint8_t address, uint8_t command);
uint8_t communication_create_data_byte(uint8_t data);


void COMMUNICATION_INIT()
{
	communication_setup_address_done = 0;
	communication_number_of_modules = 0;
	communication_current_module = 0;
	
	
	/* Configure parameters of an UART driver,
		* communication pins and install the driver */
	uart_config_t uart_config = 
	{
		.baud_rate = 57600,
		.data_bits = UART_DATA_8_BITS,
		.parity    = UART_PARITY_DISABLE,
		.stop_bits = UART_STOP_BITS_1,
		.flow_ctrl = UART_HW_FLOWCTRL_DISABLE
	};
	uart_param_config(UART_NUM_2, &uart_config);
	uart_set_pin(UART_NUM_2, 17, 16, UART_PIN_NO_CHANGE, UART_PIN_NO_CHANGE);
	if(uart_driver_install(UART_NUM_2, COMMUNICATION_UART_RX_BUFFER_SIZE * 2, 0, 0, NULL, 0) != ESP_OK)
	{
		printf("Error installing UART driver %d.\n", __LINE__);
	}
}

void COMMUNICATION_LOOP()
{
    uint8_t input_data[COMMUNICATION_UART_RX_BUFFER_SIZE];
	
	// Read data from the UART
	int len = uart_read_bytes(UART_NUM_2, input_data, COMMUNICATION_UART_RX_BUFFER_SIZE, 20 / portTICK_RATE_MS);
	
	if(communication_setup_address_done == 0)
	{
		uint8_t data[1];
		data[0] = communication_create_command_byte(0x01, COMMUNICATION_COMMAND_ASSIGN_ADDRESS);
		uart_write_bytes(UART_NUM_2, (const char *) data, 1);
		communication_setup_address_done = 1;
	}
	else if(communication_setup_address_done == 1)
	{
		for(int i = 0; i < len; i++)
		{
			if((input_data[i] & 0xF0) == 0)
			{
				if(input_data[i] & 0x0F)
				{
					communication_number_of_modules = (input_data[i] & 0x0F) - 1;
					communication_setup_address_done = 2;
					printf("Number of modules : %d.\n", communication_number_of_modules);
				}
				else
				{
					printf("Invalid number of devices received.\n");
				}
			}
			else
			{
				printf("Got char : 0x%X.\n", input_data[i]);
			}
		}
	}
	else if(communication_setup_address_done == 2)
	{
		static uint8_t data_header;
		static uint8_t data_header_received = 0;
		
		for(int i = 0; i < len; i++)
		{
			if((input_data[i] & 0b10001111) == 0) // Header for controller.
			{
				data_header = input_data[i];
				data_header_received = 1;
			}
			else if(input_data[i] & 0b10000000)
			{
				if(data_header_received)
				{
					if((data_header & 0b0111000) == (COMMUNICATION_COMMAND_RESONSE_BUTTON << 4))
					{
						uint8_t address = input_data[i] & 0x0F;
						
						if(input_data[i] & 0b00010000)
						{
							API_PARSER_SET_TURNS(communication_number_of_modules - address, 1);
						}
						else if(input_data[i] & 0b00100000)
						{
							
							API_PARSER_SET_TURNS(communication_number_of_modules - address, 0);
						}
					}
				}
				data_header_received = 0;
			}
			else
			{
				data_header_received = 0;
			}
		}
		
		
		// Make state machine for all the commands.
		if(communication_current_module == 0)
		{
			communication_current_module = communication_number_of_modules;
		}
		else
		{
			uint8_t data[2];
			uint16_t value = 0;
			int16_t status = 1;
			
			uint16_t current_module_index = communication_number_of_modules - communication_current_module;
			
			
			if(API_PARSER_GET_TURNS(current_module_index))
			{
				value = API_PARSER_GET_TURNS(current_module_index);
				status = 2;
			}
			else
			{
				value = API_PARSER_GET_BEERS(current_module_index);
				if(API_PARSER_GET_STATUS(current_module_index) >= 0)
				{
					status = 0;
				}
				else
				{
					status = 1;
				}
			}
			
			
			data[0] = communication_create_command_byte(communication_current_module, COMMUNICATION_COMMAND_DIGIT_0);
			data[1] = communication_create_data_byte(value % 10);
			uart_write_bytes(UART_NUM_2, (const char *) data, 2);
			
			data[0] = communication_create_command_byte(communication_current_module, COMMUNICATION_COMMAND_DIGIT_1);
			data[1] = communication_create_data_byte((value / 10) % 10);
			uart_write_bytes(UART_NUM_2, (const char *) data, 2);
			
			data[0] = communication_create_command_byte(communication_current_module, COMMUNICATION_COMMAND_DIGIT_2);
			data[1] = communication_create_data_byte((value / 100) % 10);
			uart_write_bytes(UART_NUM_2, (const char *) data, 2);
			
			data[0] = communication_create_command_byte(communication_current_module, COMMUNICATION_COMMAND_DIGIT_3);
			data[1] = communication_create_data_byte((value / 1000) % 10);
			uart_write_bytes(UART_NUM_2, (const char *) data, 2);
			
			data[0] = communication_create_command_byte(communication_current_module, COMMUNICATION_COMMAND_COLOUR_GREEN);
			if(status == 0)
			{
				data[1] = communication_create_data_byte(1);
			}
			else
			{
				data[1] = communication_create_data_byte(0);
			}
			uart_write_bytes(UART_NUM_2, (const char *) data, 2);
			
			data[0] = communication_create_command_byte(communication_current_module, COMMUNICATION_COMMAND_COLOUR_RED);
			if(status == 1)
			{
				data[1] = communication_create_data_byte(1);
			}
			else
			{
				data[1] = communication_create_data_byte(0);
			}
			uart_write_bytes(UART_NUM_2, (const char *) data, 2);
			
			data[0] = communication_create_command_byte(communication_current_module, COMMUNICATION_COMMAND_COLOUR_BLUE);
			if(status == 2)
			{
				data[1] = communication_create_data_byte(1);
			}
			else
			{
				data[1] = communication_create_data_byte(0);
			}
			uart_write_bytes(UART_NUM_2, (const char *) data, 2);
			
			communication_current_module--;
		}
	}
}

uint8_t communication_create_command_byte(uint8_t address, uint8_t command)
{
	uint8_t result = (address & 0x0F) | ((command << 4) & 0xF0);
	return result;
}

uint8_t communication_create_data_byte(uint8_t data)
{
	uint8_t result = data | 0b10000000;
	return result;
}
