#include "inc/global.h"
#include "inc/scheduler.h"

/* Global variables. */
volatile uint64_t system_time_ms;
volatile uint32_t system_cpu_usage;
volatile uint32_t system_logging_state;

void app_main()
{
	system_logging_state = 1;
	esp_log_level_set("*", ESP_LOG_INFO);
	printf("\n");
	printf(HASH);
	printf("              Bierlijst control\n");
	printf(HASH);
	printf(HASH);
	printf("\n");

	/* Print chip information */
	esp_chip_info_t chip_info;
	esp_chip_info(&chip_info);
	printf
	(
		"This is an ESP32 chip with %d CPU cores, WiFi%s%s, ",
		chip_info.cores,
		(chip_info.features & CHIP_FEATURE_BT) ? "/BT" : "",
		(chip_info.features & CHIP_FEATURE_BLE) ? "/BLE" : ""
	);

	printf("silicon revision %d, ", chip_info.revision);

	printf("%dMB %s flash.\n", spi_flash_get_chip_size() / (1024 * 1024), (chip_info.features & CHIP_FEATURE_EMB_FLASH) ? "embedded" : "external");

	system_time_ms = 0;
	system_cpu_usage = 100;

	while(1)
	{
		SCHEDULER_run();
	}

	printf_E("Shouldn't be here. Restarting now.");

	fflush(stdout);
	esp_restart();
}
