#include "inc/button_manager.h"

#include "inc/api_parser.h"

#define BUTTON_LED_GPIO            (25)
#define BUTTON_GPIO                (26)
#define BUTTON_LED_RESOLUTION      (1023)   // brightness 0...C_LED_RESOLUTION (13 bit mode)

ledc_channel_config_t button_led_channel;

void BUTTON_MANAGER_INIT()
{
	// Set up LED of button.
	ledc_timer_config_t ledc_timer =
	{
		.duty_resolution = LEDC_TIMER_10_BIT,      // resolution of PWM duty.
		.freq_hz         = 5000,                  // frequency of PWM signal.
		.speed_mode      = LEDC_HIGH_SPEED_MODE,   // timer mode.
		.timer_num       = LEDC_TIMER_0            // timer index.
	};
	ledc_timer_config(&ledc_timer);
	
	button_led_channel.channel       = LEDC_CHANNEL_0;
	button_led_channel.duty          = BUTTON_LED_RESOLUTION;
	button_led_channel.gpio_num      = BUTTON_LED_GPIO;
	button_led_channel.speed_mode    = LEDC_HIGH_SPEED_MODE;
	button_led_channel.timer_sel     = LEDC_TIMER_0;
	ledc_channel_config(&button_led_channel);
	ledc_set_duty(LEDC_HIGH_SPEED_MODE, button_led_channel.channel, 0);
	ledc_update_duty(LEDC_HIGH_SPEED_MODE, button_led_channel.channel);
	
	
	// Set up button input.
    gpio_config_t io_conf;
    io_conf.intr_type = GPIO_PIN_INTR_DISABLE;
    io_conf.pin_bit_mask = 0b1 << BUTTON_GPIO;
    io_conf.mode = GPIO_MODE_INPUT;
    io_conf.pull_up_en = 1;
    io_conf.pull_down_en = 0;
    gpio_config(&io_conf);	
}

void BUTTON_MANAGER_LOOP()
{
	const uint16_t fade_speed = 40;
	static uint32_t last_led_value = 0;
	uint8_t led_state = 0;
	for(uint16_t i = 0; i < API_PARSER_CONSTANT_NUMBER_OF_PEOPLE; i++)
	{
		if(API_PARSER_GET_TURNS(i) != 0)
		{
			led_state = 1;
			break;
		}
	}
	if(led_state)
	{
		if(last_led_value <= (BUTTON_LED_RESOLUTION - fade_speed))
		{
			last_led_value = last_led_value + fade_speed;
			ledc_set_duty(LEDC_HIGH_SPEED_MODE, button_led_channel.channel, last_led_value);
			ledc_update_duty(LEDC_HIGH_SPEED_MODE, button_led_channel.channel);
		}
	}
	else
	{
		if(last_led_value > fade_speed)
		{
			last_led_value = last_led_value - fade_speed;
			if(last_led_value <= fade_speed)
			{
				last_led_value = 0;
			}
			ledc_set_duty(LEDC_HIGH_SPEED_MODE, button_led_channel.channel, last_led_value);
			ledc_update_duty(LEDC_HIGH_SPEED_MODE, button_led_channel.channel);
		}
	}
	
	
	
	static uint32_t press_latch = 0;
	if(gpio_get_level(BUTTON_GPIO) == 0)
	{
		press_latch = 1;
	}
	else if(press_latch != 0)
	{
		press_latch = 0;
		printf("Button pressed.\n");
		API_PARSER_HANDLE_BUTTON();
	}
}
