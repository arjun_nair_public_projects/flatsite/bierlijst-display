#ifndef _GPIO_H_
#define _GPIO_H_

#include "../global.h"
typedef enum e_gpio_port
{
	PORT_A,
	PORT_B,
	PORT_C,
	PORT_D,
	PORT_E,
	PORT_F
} t_gpio_port;

typedef struct s_gpio
{
	t_gpio_port port;
	int pin;
	
	volatile unsigned char *port_odr;
	volatile unsigned char *port_idr;
	volatile unsigned char *port_ddr;
	volatile unsigned char *port_cr1;
	volatile unsigned char *port_cr2;
} t_gpio;


void GPIO_INIT_STRUCT(enum e_gpio_port port, int pin, t_gpio *result_gpio);
void GPIO_SET_DIRECTION_OUTPUT(t_gpio *gpio);
void GPIO_SET_DIRECTION_INPUT(t_gpio *gpio);
void GPIO_SET_OUTPUT(t_gpio *gpio, uint8_t output);
uint8_t GPIO_GET_INPUT(t_gpio *gpio);

#endif /* _GPIO_H_ */ 
