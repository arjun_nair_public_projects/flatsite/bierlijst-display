#include "./gpio.h"

#include "./bit.h"


void GPIO_INIT_STRUCT(enum e_gpio_port port, int pin, t_gpio *result_gpio)
{
	switch(port)
	{
	case PORT_A:
		result_gpio->port_odr = &(PA_ODR);
		result_gpio->port_idr = &(PA_IDR);
		result_gpio->port_ddr = &(PA_DDR);
		result_gpio->port_cr1 = &(PA_CR1);
		result_gpio->port_cr2 = &(PA_CR2);
		break;
	case PORT_B:
		result_gpio->port_odr = &(PB_ODR);
		result_gpio->port_idr = &(PB_IDR);
		result_gpio->port_ddr = &(PB_DDR);
		result_gpio->port_cr1 = &(PB_CR1);
		result_gpio->port_cr2 = &(PB_CR2);
		break;
	case PORT_C:
		result_gpio->port_odr = &(PC_ODR);
		result_gpio->port_idr = &(PC_IDR);
		result_gpio->port_ddr = &(PC_DDR);
		result_gpio->port_cr1 = &(PC_CR1);
		result_gpio->port_cr2 = &(PC_CR2);
		break;
	case PORT_D:
		result_gpio->port_odr = &(PD_ODR);
		result_gpio->port_idr = &(PD_IDR);
		result_gpio->port_ddr = &(PD_DDR);
		result_gpio->port_cr1 = &(PD_CR1);
		result_gpio->port_cr2 = &(PD_CR2);
		break;
	case PORT_E:
		result_gpio->port_odr = &(PE_ODR);
		result_gpio->port_idr = &(PE_IDR);
		result_gpio->port_ddr = &(PE_DDR);
		result_gpio->port_cr1 = &(PE_CR1);
		result_gpio->port_cr2 = &(PE_CR2);
		break;
	case PORT_F:
		result_gpio->port_odr = &(PF_ODR);
		result_gpio->port_idr = &(PF_IDR);
		result_gpio->port_ddr = &(PF_DDR);
		result_gpio->port_cr1 = &(PF_CR1);
		result_gpio->port_cr2 = &(PF_CR2);
		break;
	}
	
	result_gpio->port = port;
	result_gpio->pin  = pin;
}


void GPIO_SET_DIRECTION_OUTPUT(t_gpio *gpio)
{
	BIT_SET_REG_BIT_POS((*(gpio->port_ddr)), gpio->pin);
}

void GPIO_SET_DIRECTION_INPUT(t_gpio *gpio)
{
	BIT_UNSET_REG_BIT_POS((*(gpio->port_ddr)), gpio->pin);
}

void GPIO_SET_OUTPUT(t_gpio *gpio, uint8_t output)
{
	if(output)
	{
		BIT_SET_REG_BIT_POS((*(gpio->port_odr)), gpio->pin);
	}
	else
	{
		BIT_UNSET_REG_BIT_POS((*(gpio->port_odr)), gpio->pin);
	}
}

uint8_t GPIO_GET_INPUT(t_gpio *gpio)
{
	uint8_t val = (*(gpio->port_idr)) & (0b1 << gpio->pin);
	if(val)
	{
		return 1;
	}
	else
	{
		return 0;
	}
}
