#ifndef _BIT_H_
#define _BIT_H_

#include "../global.h"


#define BIT_SET_REG_BIT_POS(_REG_, _POS_)        (_REG_) |= (uint8_t)(0b1 << (_POS_))
#define BIT_UNSET_REG_BIT_POS(_REG_, _POS_)      (_REG_) &= ~((uint8_t)(0b1 << (_POS_)))

#endif /* _BIT_H_ */  
