#include "./global.h"
#include "./main_code/button_controller.h"
#include "./main_code/communication_application.h"
#include "./main_code/display_controller.h"
#include "./main_code/led_controller.h"

void main()
{
	// Setup clock.
	CLK_CKDIVR = 0;
	
	// Init functions.
	BUTTON_CONTROLLER_INIT();
	COMMUNICATION_APPLICATION_INIT();
	DISPLAY_CONTROLLER_INIT();
	LED_CONTROLLER_INIT();
	
	while (1)
	{
		BUTTON_CONTROLLER_LOOP();
		COMMUNICATION_APPLICATION_LOOP();
		DISPLAY_CONTROLLER_LOOP();
		LED_CONTROLLER_LOOP();
	}
}
