#include "./display_controller.h"

#include "../utilities/gpio.h"


void display_controller_blank(void);
uint8_t display_controller_get_bitmap(uint8_t number);


uint8_t display_digit[4];

t_gpio display_gpio_disp_element[4];
t_gpio display_gpio_disp_character[8]; // a, b, c, d, e, f, g, dp.

void DISPLAY_CONTROLLER_INIT()
{
	int i;
	
	// Initialise structures.
	GPIO_INIT_STRUCT(PORT_D, 7, &display_gpio_disp_element[0]);
	GPIO_INIT_STRUCT(PORT_F, 4, &display_gpio_disp_element[1]);
	GPIO_INIT_STRUCT(PORT_B, 7, &display_gpio_disp_element[2]);
	GPIO_INIT_STRUCT(PORT_B, 6, &display_gpio_disp_element[3]);
	GPIO_INIT_STRUCT(PORT_A, 1, &display_gpio_disp_character[0]);
	GPIO_INIT_STRUCT(PORT_B, 5, &display_gpio_disp_character[1]);
	GPIO_INIT_STRUCT(PORT_D, 2, &display_gpio_disp_character[2]);
	GPIO_INIT_STRUCT(PORT_D, 3, &display_gpio_disp_character[3]);
	GPIO_INIT_STRUCT(PORT_C, 6, &display_gpio_disp_character[4]);
	GPIO_INIT_STRUCT(PORT_A, 2, &display_gpio_disp_character[5]);
	GPIO_INIT_STRUCT(PORT_A, 3, &display_gpio_disp_character[6]);
	GPIO_INIT_STRUCT(PORT_D, 4, &display_gpio_disp_character[7]);
	
	// Set GPIO output registers.
	display_controller_blank();
	
	// Set output configuration as push pull.
	for(i = 0; i < 4; i++)
	{
		*(display_gpio_disp_element[i].port_cr1) |= (0b1 << display_gpio_disp_element[i].pin);
	}
	for(i = 0; i < 8; i++)
	{
		*(display_gpio_disp_character[i].port_cr1) |= (0b1 << display_gpio_disp_character[i].pin);
	}
	
	// Set pin direction as output.
	for(i = 0; i < 4; i++)
	{
		GPIO_SET_DIRECTION_OUTPUT(&display_gpio_disp_element[i]);
	}
	for(i = 0; i < 8; i++)
	{
		GPIO_SET_DIRECTION_OUTPUT(&display_gpio_disp_character[i]);
	}
	
	for(i = 0; i < 4; i++)
	{
		display_digit[i] = 0;
	}
}

void DISPLAY_CONTROLLER_LOOP()
{
	static uint16_t interval_counter = 0;
	static uint16_t element_number = 0;
	static uint16_t character_number = 0;
	uint16_t i;
	
	const uint16_t interval = 2;
	
	if(interval_counter >= interval)
	{
		element_number++;
		interval_counter = 0;
	}
	if(element_number >= 4)
	{
		character_number++;
		element_number = 0;
		
		for(i = 0; i < 8; i++)
		{
			GPIO_SET_OUTPUT(&display_gpio_disp_character[i], 1);
		}
	}
	if(character_number >= 8)
	{
		character_number = 0;
	}

	if(display_controller_get_bitmap(display_digit[element_number]) & (0b10000000 >> character_number))
	{
		for(i = 0; i < 4; i++)
		{
			if(i == element_number)
			{
				GPIO_SET_OUTPUT(&display_gpio_disp_element[i], 1);
			}
			else
			{
				GPIO_SET_OUTPUT(&display_gpio_disp_element[i], 0);
			}
		}
	}
	else
	{
		for(i = 0; i < 4; i++)
		{
			GPIO_SET_OUTPUT(&display_gpio_disp_element[i], 0);
		}
	}
	
	for(i = 0; i < 8; i++)
	{
		if(i == character_number)
		{
			GPIO_SET_OUTPUT(&display_gpio_disp_character[i], 0);
		}
		else
		{
			GPIO_SET_OUTPUT(&display_gpio_disp_character[i], 1);
		}
	}
	
	interval_counter++;
}

void DISPLAY_CONTROLLER_SET_DIGITS(uint8_t digit3, uint8_t digit2, uint8_t digit1, uint8_t digit0)
{
	display_digit[3] = digit0;
	display_digit[2] = digit1;
	display_digit[1] = digit2;
	display_digit[0] = digit3;
}

void DISPLAY_CONTROLLER_SET_DIGIT(uint8_t digit, uint8_t value)
{
	display_digit[digit] = value;
}


void display_controller_blank()
{
	int i;
	for(i = 0; i < 4; i++)
	{
		GPIO_SET_OUTPUT(&display_gpio_disp_element[i], 0);
	}
	for(i = 0; i < 8; i++)
	{
		GPIO_SET_OUTPUT(&display_gpio_disp_character[i], 1);
	}
}

uint8_t display_controller_get_bitmap(uint8_t number)
{
	uint8_t bitmap = 0;
	
	switch(number)
	{
	case 0:
		bitmap = 0b11111100;
		break;
	case 1:
		bitmap = 0b01100000;
		break;
	case 2:
		bitmap = 0b11011010;
		break;
	case 3:
		bitmap = 0b11110010;
		break;
	case 4:
		bitmap = 0b01100110;
		break;
	case 5:
		bitmap = 0b10110110;
		break;
	case 6:
		bitmap = 0b10111110;
		break;
	case 7:
		bitmap = 0b11100000;
		break;
	case 8:
		bitmap = 0b11111110;
		break;
	case 9:
		bitmap = 0b11110110;
		break;
	default:
		bitmap = 0b10011110;
		break;
	}
	return bitmap;
}
