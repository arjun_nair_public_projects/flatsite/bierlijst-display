#include "./communication_application.h"

#include "./display_controller.h"
#include "./led_controller.h"

#define COMMUNICATION_SEND_BUFFER_SIZE     (10)


// Commands.
#define COMMUNICATION_COMMAND_MASK               (0b01110000)
#define COMMUNICATION_ADDRESS_MASK               (0b00001111)
#define COMMUNICATION_COMMAND_ASSIGN_ADDRESS     (0x0 << 4)
#define COMMUNICATION_COMMAND_DIGIT_0            (0x1 << 4)
#define COMMUNICATION_COMMAND_DIGIT_1            (0x2 << 4)
#define COMMUNICATION_COMMAND_DIGIT_2            (0x3 << 4)
#define COMMUNICATION_COMMAND_DIGIT_3            (0x4 << 4)
#define COMMUNICATION_COMMAND_COLOUR_RED         (0x5 << 4)
#define COMMUNICATION_COMMAND_COLOUR_GREEN       (0x6 << 4)
#define COMMUNICATION_COMMAND_COLOUR_BLUE        (0x7 << 4)
// Response.
#define COMMUNICATION_COMMAND_RESONSE_BUTTON     (0x1 << 4)


@near uint8_t communication_send_buffer[COMMUNICATION_SEND_BUFFER_SIZE];
uint16_t communication_send_buffer_put, communication_send_buffer_get;
uint8_t communication_my_address;
uint16_t communication_button_turns;
uint8_t communication_button_header_sent;
volatile uint8_t communication_last_command, communication_expecting_data;

void COMMUNICATION_APPLICATION_INIT()
{
	communication_send_buffer_get = 0;
	communication_send_buffer_put = 0;
	
	communication_my_address = 0;
	
	communication_button_turns = 0;
	communication_button_header_sent = 0;
	
	communication_last_command = 0;
	communication_expecting_data = 0;
	
	UART1_SR    = 0;
	UART1_BRR1  = 0x11;
	UART1_BRR2  = 0x06;
	UART1_CR1   = 0b00000000;
	UART1_CR2   = 0b00101100;
	UART1_CR3   = 0b00000000;
	UART1_CR4   = 0b00000000;
	UART1_CR5   = 0b00000000;
	UART1_GTR   = 0;
	
	ITC_SPR5    = 0b11011111;
	
	// Enable interrupts.
	_asm("RIM");
}

void COMMUNICATION_APPLICATION_LOOP()
{
	static uint8_t a = 0;
	
	if(UART1_SR & 0b10000000)
	{
		if(communication_button_turns)
		{
			if(communication_button_header_sent == 0)
			{
				UART1_DR = COMMUNICATION_COMMAND_RESONSE_BUTTON;
				communication_button_header_sent = 1;
			}
			else
			{
				UART1_DR = 0b10000000 | (communication_button_turns << 4) | communication_my_address;
				communication_button_header_sent = 0;
				communication_button_turns = 0;
			}
		}
		else if(communication_send_buffer_get != communication_send_buffer_put)
		{
			UART1_DR = communication_send_buffer[communication_send_buffer_get];
			communication_send_buffer_get = (communication_send_buffer_get + 1) % COMMUNICATION_SEND_BUFFER_SIZE;
		}
	}
}

void COMMUNICATION_APPLICATION_BUTTON_UP()
{
	communication_button_turns = 1;
}


void COMMUNICATION_APPLICATION_BUTTON_DOWN()
{
	communication_button_turns = 2;
}


void UART_RX_INTERRUPT_HANDLER()
{
	uint8_t x = UART1_DR;
	
	// It is a command.
	if((x & 0b10000000) == 0)
	{
		if((x & COMMUNICATION_COMMAND_MASK) == COMMUNICATION_COMMAND_ASSIGN_ADDRESS)
		{
			communication_my_address = x & COMMUNICATION_ADDRESS_MASK;
			communication_send_buffer[communication_send_buffer_put] = x + 1;
			communication_send_buffer_put = (communication_send_buffer_put + 1) % COMMUNICATION_SEND_BUFFER_SIZE;
		}
		else
		{
			communication_last_command = x;
			communication_expecting_data = 1;
		}
	}
	// It is data.
	else if(communication_expecting_data)
	{
		uint8_t data = x & 0b01111111;
		communication_expecting_data = 0;
		
		if((communication_last_command & COMMUNICATION_ADDRESS_MASK) == communication_my_address)
		{
			communication_last_command = communication_last_command & COMMUNICATION_COMMAND_MASK;
			
			if(communication_last_command == COMMUNICATION_COMMAND_DIGIT_0)
			{
				DISPLAY_CONTROLLER_SET_DIGIT(3, data);
			}
			else if(communication_last_command == COMMUNICATION_COMMAND_DIGIT_1)
			{
				DISPLAY_CONTROLLER_SET_DIGIT(2, data);
			}
			else if(communication_last_command == COMMUNICATION_COMMAND_DIGIT_2)
			{
				DISPLAY_CONTROLLER_SET_DIGIT(1, data);
			}
			else if(communication_last_command == COMMUNICATION_COMMAND_DIGIT_3)
			{
				DISPLAY_CONTROLLER_SET_DIGIT(0, data);
			}
			else if(communication_last_command == COMMUNICATION_COMMAND_COLOUR_RED)
			{
				LED_CONTROLLER_SET_COLOUR_RED(data);
			}
			else if(communication_last_command == COMMUNICATION_COMMAND_COLOUR_GREEN)
			{
				LED_CONTROLLER_SET_COLOUR_GREEN(data);
			}
			else if(communication_last_command == COMMUNICATION_COMMAND_COLOUR_BLUE)
			{
				LED_CONTROLLER_SET_COLOUR_BLUE(data);
			}
		}
		else
		{
			communication_send_buffer[communication_send_buffer_put] = communication_last_command;
			communication_send_buffer_put = (communication_send_buffer_put + 1) % COMMUNICATION_SEND_BUFFER_SIZE;
			communication_send_buffer[communication_send_buffer_put] = x;
			communication_send_buffer_put = (communication_send_buffer_put + 1) % COMMUNICATION_SEND_BUFFER_SIZE;
		}
	}
}
