#ifndef _LED_CONTROLLER_H_
#define _LED_CONTROLLER_H_

#include "../global.h"

void LED_CONTROLLER_INIT(void);
void LED_CONTROLLER_LOOP(void);
void LED_CONTROLLER_SET_COLOURS(uint8_t red, uint8_t green, uint8_t blue);
void LED_CONTROLLER_SET_COLOUR_RED(uint8_t value);
void LED_CONTROLLER_SET_COLOUR_GREEN(uint8_t value);
void LED_CONTROLLER_SET_COLOUR_BLUE(uint8_t value);

#endif /* _LED_CONTROLLER_H_ */
