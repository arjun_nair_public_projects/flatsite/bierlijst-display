#include "./led_controller.h"

#include "../utilities/gpio.h"

uint8_t led_red;
uint8_t led_green;
uint8_t led_blue;

t_gpio led_gpio_red;
t_gpio led_gpio_green;
t_gpio led_gpio_blue;

void LED_CONTROLLER_INIT()
{
	led_red   = 0;
	led_green = 0;
	led_blue  = 0;
	
	// Initialise structures.
	GPIO_INIT_STRUCT(PORT_B, 0, &led_gpio_red);
	GPIO_INIT_STRUCT(PORT_B, 1, &led_gpio_green);
	GPIO_INIT_STRUCT(PORT_B, 2, &led_gpio_blue);
	
	// Set GPIO output registers.
	GPIO_SET_OUTPUT(&led_gpio_red,   0);
	GPIO_SET_OUTPUT(&led_gpio_green, 0);
	GPIO_SET_OUTPUT(&led_gpio_blue,  0);
	
	// Set output configuration as push pull.
	*(led_gpio_red.port_cr1)   |= (0b1 << led_gpio_red.pin);
	*(led_gpio_green.port_cr1) |= (0b1 << led_gpio_green.pin);
	*(led_gpio_blue.port_cr1)  |= (0b1 << led_gpio_blue.pin);
	
	// Set pin direction as output.
	GPIO_SET_DIRECTION_OUTPUT(&led_gpio_red);
	GPIO_SET_DIRECTION_OUTPUT(&led_gpio_green);
	GPIO_SET_DIRECTION_OUTPUT(&led_gpio_blue);
}

void LED_CONTROLLER_LOOP()
{	
	if(led_red)
	{
		GPIO_SET_OUTPUT(&led_gpio_red, 1);
	}
	else
	{
		GPIO_SET_OUTPUT(&led_gpio_red, 0);
	}
	
	if(led_green)
	{
		GPIO_SET_OUTPUT(&led_gpio_green, 1);
	}
	else
	{
		GPIO_SET_OUTPUT(&led_gpio_green, 0);
	}
	
	if(led_blue)
	{
		GPIO_SET_OUTPUT(&led_gpio_blue, 1);
	}
	else
	{
		GPIO_SET_OUTPUT(&led_gpio_blue, 0);
	}
	
	
}

void LED_CONTROLLER_SET_COLOURS(uint8_t red, uint8_t green, uint8_t blue)
{
	led_red    = red;
	led_green  = green;
	led_blue   = blue;
}

void LED_CONTROLLER_SET_COLOUR_RED(uint8_t value)
{
	led_red    = value;
}

void LED_CONTROLLER_SET_COLOUR_GREEN(uint8_t value)
{
	led_green    = value;
}

void LED_CONTROLLER_SET_COLOUR_BLUE(uint8_t value)
{
	led_blue    = value;
}
