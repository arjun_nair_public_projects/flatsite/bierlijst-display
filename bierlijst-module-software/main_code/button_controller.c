#include "./button_controller.h"

#include "../utilities/bit.h"
#include "../utilities/gpio.h"

#include "./communication_application.h"
#include "./display_controller.h"

t_gpio button_rotary_a;
t_gpio button_rotary_b;

uint16_t temp_number = 420;

void BUTTON_CONTROLLER_INIT()
{
	// Initialise structures.
	GPIO_INIT_STRUCT(PORT_D, 0, &button_rotary_a);
	GPIO_INIT_STRUCT(PORT_C, 7, &button_rotary_b);
	
	// Set GPIO output registers.
	GPIO_SET_OUTPUT(&button_rotary_a,   0);
	GPIO_SET_OUTPUT(&button_rotary_b, 0);
	
	// Set input configuration as floating.
	BIT_UNSET_REG_BIT_POS((*(button_rotary_a.port_cr1)), button_rotary_a.pin);
	BIT_UNSET_REG_BIT_POS((*(button_rotary_b.port_cr1)), button_rotary_b.pin);
	
	// Set pin direction as output.
	GPIO_SET_DIRECTION_INPUT(&button_rotary_a);
	GPIO_SET_DIRECTION_INPUT(&button_rotary_b);
}

void BUTTON_CONTROLLER_LOOP()
{
	const uint16_t debounce_time = 10;
	static uint16_t debounce_counter = 0;
	static uint8_t debounce_latch =0;
	
	uint8_t current_a;
	uint8_t current_b;
	
	current_a = GPIO_GET_INPUT(&button_rotary_a);
	current_b = GPIO_GET_INPUT(&button_rotary_b);

	
	if(current_a == 0)
	{
		if(debounce_latch == 0)
		{
			debounce_counter++;
			if(debounce_counter > debounce_time)
			{
				if(current_b)
				{
					temp_number++;
					COMMUNICATION_APPLICATION_BUTTON_UP();
				}
				else
				{
					temp_number--;
					COMMUNICATION_APPLICATION_BUTTON_DOWN();
				}
				debounce_counter = 0;
				debounce_latch = 1;
			}
		}
	}
	else
	{
		debounce_latch = 0;
	}
		
	//DISPLAY_CONTROLLER_SET_DIGITS((temp_number / 1000) % 10, (temp_number / 100) % 10, (temp_number / 10) % 10, temp_number % 10);
}
