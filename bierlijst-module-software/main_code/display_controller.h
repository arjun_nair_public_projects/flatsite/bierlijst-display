#ifndef _DISPLAY_CONTROLLER_H_
#define _DISPLAY_CONTROLLER_H_

#include "../global.h"

void DISPLAY_CONTROLLER_INIT(void);
void DISPLAY_CONTROLLER_LOOP(void);
void DISPLAY_CONTROLLER_SET_DIGITS(uint8_t digit3, uint8_t digit2, uint8_t digit1, uint8_t digit0);
void DISPLAY_CONTROLLER_SET_DIGIT(uint8_t digit, uint8_t value);

#endif /* _DISPLAY_CONTROLLER_H_ */
