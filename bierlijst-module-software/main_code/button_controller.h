#ifndef _BUTTON_CONTROLLER_H_
#define _BUTTON_CONTROLLER_H_

#include "../global.h"


void BUTTON_CONTROLLER_INIT(void);
void BUTTON_CONTROLLER_LOOP(void);

#endif /* _BUTTON_CONTROLLER_H_ */
